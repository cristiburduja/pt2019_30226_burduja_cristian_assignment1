package Control;

import Packk.Monom;
import Packk.Polinom;
import View.TestPolinom;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AdunarePolinom implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent e) {

        String inputText = TestPolinom.getInputValue().getText();

        String inputPlusText = TestPolinom.getInputPlus().getText();

        Polinom inputTextArray;
        Polinom inputPlusArray;


        inputTextArray = TestPolinom.conversion(inputText);
        inputPlusArray = TestPolinom.conversion(inputPlusText);

       //  int plusArrayLength = Math.max(inputTextArray.getMonoame().size(), inputPlusArray.getMonoame().size());
        Polinom  outputArray = new Polinom();

        for(Monom i:inputTextArray.getMonoame()){
                outputArray.adauga(i); // inputtextaray e exact nr meu , adica coeficientu
        }
        for(Monom i:inputPlusArray.getMonoame()){
            outputArray.adauga(i);
        }

        outputArray.addCoef();

        TestPolinom.showPolynomial(outputArray);

    }
}


