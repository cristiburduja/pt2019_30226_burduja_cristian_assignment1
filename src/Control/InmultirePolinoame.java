package Control;

        import Packk.Monom;
        import Packk.Polinom;
        import View.TestPolinom;

        import java.awt.event.ActionEvent;
        import java.awt.event.ActionListener;

public class InmultirePolinoame implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent e) {
        String inputText = TestPolinom.getInputValue().getText();

        String inputInmultirePolinomText = TestPolinom.getInputMultiplyPolynomial().getText();

        Polinom inputTextArray;
        Polinom inputInmultirePolinomArray;


        inputTextArray = TestPolinom.conversion(inputText);
        inputInmultirePolinomArray = TestPolinom.conversion(inputInmultirePolinomText);
        for(Monom i:inputTextArray.getMonoame())
            System.out.println(i);
        for(Monom i:inputInmultirePolinomArray.getMonoame())
            System.out.println(i);
        //  int plusArrayLength = Math.max(inputTextArray.getMonoame().size(),inputInmultirePolinomArray.getMonoame().size());
        Polinom  outputArray = new Polinom();

        for(Monom i:inputTextArray.getMonoame())
            for(Monom j:inputInmultirePolinomArray.getMonoame())
            {
                outputArray.adauga(new Monom(i.getPutere()+j.getPutere(),i.getCoeficient()*j.getCoeficient()));
            }

        outputArray.addCoef();
        TestPolinom.showPolynomial(outputArray);
    }
}


