package Control;

import Packk.Monom;
import Packk.Polinom;
import View.TestPolinom;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class IntegralaPolinom implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent e) {


        String inputText = TestPolinom.getInputValue().getText();



        Polinom inputTextArray;

        inputTextArray = TestPolinom.conversion(inputText);

        Polinom  outputArray = new Polinom();

        for(Monom i:inputTextArray.getMonoame())
        {
            outputArray.adauga(new Monom(i.getPutere()+1,i.getCoeficient()/(i.getPutere()+1)));
        }

        outputArray.addCoef();

        TestPolinom.showPolynomial(outputArray);
        }
    }


