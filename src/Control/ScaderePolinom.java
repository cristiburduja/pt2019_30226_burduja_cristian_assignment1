package Control;

import Packk.Monom;
import Packk.Polinom;
import View.TestPolinom;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ScaderePolinom implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent e) {
        String inputText = TestPolinom.getInputValue().getText();
        String inputMinusText = TestPolinom.getInputMinus().getText();

        Polinom inputTextArray;
        Polinom inputMinusArray;

        inputTextArray = TestPolinom.conversion(inputText);
        inputMinusArray = TestPolinom.conversion(inputMinusText);

        Polinom  outputArray = new Polinom();

        for(Monom i:inputTextArray.getMonoame()){
            outputArray.adauga(i); // inputtextaray e exact nr meu , adica coeficientu
        }
        for(Monom i:inputMinusArray.getMonoame()){
            outputArray.adauga(new Monom(i.getPutere(),i.getCoeficient()*-1));
        }
        outputArray.addCoef();

        TestPolinom.showPolynomial(outputArray);
    }
}
