package Packk;

public class Monom implements Comparable <Monom>{
    private int putere;
    private float coeficient;

    public int getPutere() {
        return putere;
    }

    public void setPutere(int putere) {
        this.putere = putere;
    }

    public float getCoeficient() {
        return coeficient;
    }

    public void setCoeficient(float coeficient) {
        this.coeficient = coeficient;
    }

    public Monom(int putere, float coeficent)
    {
        this.putere=putere;
        this.coeficient=coeficent;



    }

    public String toString(){
        return coeficient + "x^" + putere;
    }

    @Override
    public int compareTo(Monom o) {
        if(this.putere>o.putere)
            return -1; // returneaza -1 cand vrei sa ordoneze DESCREScator
        if(this.putere<o.putere)
            return 1;
        return 0;
    }
}
