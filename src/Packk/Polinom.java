package Packk;

import java.util.ArrayList;
import java.util.Collections;

public class Polinom {
    private  ArrayList<Monom> monoame; // respect paradigma oop numita incapsulare


    public Polinom()
    {

        monoame=new ArrayList<Monom>();

    }

    public void addCoef(){
        Collections.sort(monoame);
        int i=0;
        while(i<monoame.size())
        {
            Monom m=monoame.get(i);
            i++;
            while(i<monoame.size() && m.getPutere()==this.monoame.get(i).getPutere()){ // parcurg toate monoamele dupa primul si verific puterile
                m.setCoeficient(m.getCoeficient()+this.monoame.get(i).getCoeficient());
                this.stergere(this.monoame.get(i));
            }
        }

        for(i=0;i<monoame.size();i++)
            if(monoame.get(i).getCoeficient()==0) {
                this.stergere(monoame.get(i));
                i--;
            }

    }

    public void adauga(Monom m){
        monoame.add(m);
    }
    public void stergere(Monom m){
        monoame.remove(m);
    }

    public ArrayList<Monom> getMonoame() {
        return monoame;
    }

    public void setMonoame(ArrayList<Monom> monoame) {
        this.monoame = monoame;
    }
}
