package View;

import Control.*;
import Packk.*;

import javax.swing.*;

public class TestPolinom extends JFrame
{

    static JTextField inputValue;
    static JTextField inputPlus;

    static JTextField inputMinus;

    public static JTextField getInputValue() {
        return inputValue;
    }

    public static JTextField getInputPlus() {
        return inputPlus;
    }

    public static JTextField getInputMinus() {
        return inputMinus;
    }

    public static JTextField getInputMultiplyPolynomial() {
        return inputMultiplyPolynomial;


    }

    static JTextField inputMultiplyPolynomial;  //TextFielduri pt introducerea polinoamelor


    static JButton buttonPlus, buttonMinus,  buttonIntegrate, buttonDerivate, buttonMultiplyPolynomial;

    static JLabel input, output, labelPolynomial,  labelProjectSentence2;
    static JTextArea inputShow, outputShow;       // text area pt afisarea polinomului citit si cel afisat

    public TestPolinom() {
        setTitle("Operatii polinoame ");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(600, 350);
        setVisible(true);
    }

    public void init() {

        this.setLayout(null);


        input = new JLabel("Dati primul polinom");
        input.setBounds(0, 0, 1000, 20);
        inputValue = new JTextField();
        inputValue.setBounds(150, 0, 300, 20);
        add(input);
        add(inputValue);

        labelPolynomial = new JLabel("Polinomul e :");
        labelPolynomial.setBounds(0,20,100,20);
        inputShow = new JTextArea();
        inputShow.setBounds(150, 20, 300, 20);
        inputShow.setEditable(false);
        add(inputShow);
        add(labelPolynomial);

        buttonPlus = new JButton("Plus");
        buttonPlus.setBounds(0, 40, 150, 20);
        inputPlus = new JTextField();
        inputPlus.setBounds(150, 40, 300, 20);
        add(buttonPlus);
        add(inputPlus);
        buttonPlus.addActionListener(new AdunarePolinom());

        buttonMinus = new JButton("Minus");
        buttonMinus.setBounds(0, 60, 150, 20);
        inputMinus = new JTextField();
        inputMinus.setBounds(150, 60, 300, 20);
        add(buttonMinus);
        add(inputMinus);
        buttonMinus.addActionListener(new ScaderePolinom());


        buttonIntegrate = new JButton("Integrare");
        buttonIntegrate.setBounds(0, 120, 150, 20);
        add(buttonIntegrate);
        buttonIntegrate.addActionListener(new IntegralaPolinom());

        buttonDerivate = new JButton("Derivare");
        buttonDerivate.setBounds(0, 140, 150, 20);
        add(buttonDerivate);
        buttonDerivate.addActionListener(new DerivarePolinom());

        buttonMultiplyPolynomial = new JButton("Inmultire");
        buttonMultiplyPolynomial.setBounds(0, 160, 150, 20);
        inputMultiplyPolynomial = new JTextField();
        inputMultiplyPolynomial.setBounds(150, 160, 300, 20);
        add(buttonMultiplyPolynomial);
        add(inputMultiplyPolynomial);
        buttonMultiplyPolynomial.addActionListener(new InmultirePolinoame());



        output = new JLabel("Rezultat ");
        output.setBounds(0, 200, 100, 20);
        outputShow = new JTextArea();
        outputShow.setBounds(150, 200, 300, 20);
        outputShow.setEditable(false);
        add(output);
        add(outputShow);


        labelProjectSentence2 = new JLabel("Introdu doar coeficientii cu spatii intre");
        labelProjectSentence2.setBounds(0, 240, 300, 20);


        add(labelProjectSentence2);

    }


    public static Polinom conversion(String polynomialString) {
        polynomialString=polynomialString.replaceAll("-","+-");
        Polinom rez=new Polinom();
        if(polynomialString.charAt(0)=='+')
            polynomialString=polynomialString.substring(1);
        String[] monoame = polynomialString.split("\\+");
        for(int i=0;i<monoame.length;i++){
            String[] splitter = monoame[i].split("x\\^");
            //System.out.println(splitter[0]);
            //System.out.println(splitter[1]);

            try {
                Monom m = new Monom(Integer.parseInt(splitter[1]), Integer.parseInt(splitter[0]));
                rez.adauga(m);
            }
            catch(Exception e){
                JOptionPane.showMessageDialog(null,"Polinom introdus invalid");
                e.printStackTrace();
            }
        }

        return rez;
    }

    public static void showPolynomial(Polinom arrayToShow) {
        String s="";
        for(int i=0;i<arrayToShow.getMonoame().size();i++)
        {
                if(i+1==arrayToShow.getMonoame().size())
                    s+=arrayToShow.getMonoame().get(i).toString();
                else
                    s+=arrayToShow.getMonoame().get(i).toString() + "+";

        }
        if(s.equals(""))
            outputShow.setText("0");
        else
            outputShow.setText(s);

    }



}
